use crate::render_graph::{NodeInterface, RenderNode};
use crate::render_graph::connexion::ConnexionData;
use crate::gl::Gl;

pub struct ValueNode
{
    input_value: NodeInterface,
    output_value: NodeInterface,
}

impl ValueNode {
    pub fn new (init_value: ConnexionData) -> Self {
        Self {
            input_value: NodeInterface {
                name: "in_value".to_string(),
                data: Some(init_value),
            },
            output_value: NodeInterface {
                name: "out_value".to_string(),
                data: None,
            },
        }
    }
}

impl RenderNode for ValueNode {
    fn evaluate(&mut self, _gl: &Gl) {
        // nothing to do
    }

    fn update(&mut self, _gl: &Gl, _delta_time: f32) {
        self.output_value.data = self.input_value.data.clone();
    }

    fn get_input_names(&self) -> Vec<String> {
        vec!(self.input_value.name.clone())
    }

    fn set_input(&mut self, name: String, value: Option<ConnexionData>) {
        if name == self.input_value.name && value.is_some() {
            self.input_value.data = value;
        }
    }

    fn get_output_names(&self) -> Vec<String> {
        vec!(self.output_value.name.clone())
    }

    fn get_output(&self, name: String) -> Option<&NodeInterface> {
        if name == self.output_value.name {
            Some(&self.output_value)
        } else {
            None
        }
    }
}