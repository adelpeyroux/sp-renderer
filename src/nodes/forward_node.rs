use crate::render_graph::{NodeInterface, RenderNode};
use crate::gl::Gl;
use crate::render_graph::connexion::ConnexionData;
use crate::render_graph::connexion::ConnexionData::{RenderTarget as RenderTargetData, Boolean, Float};
use crate::gl::gl_enum::{GL_BLEND, GL_DEPTH_TEST, GL_VIEWPORT, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, GL_STENCIL_BUFFER_BIT, GLsizei};
use crate::sp_scene::sp_mesh::sp_math::vector::Vector2f;
use crate::graphical_objects::render_target::RenderTarget;

pub struct ForwardNode {
    win_size: Vector2f,
    in_shader: NodeInterface,
    in_scene: NodeInterface,
    in_camera: NodeInterface,
    in_wireframe: NodeInterface,
    in_soft: NodeInterface,
    in_tess_level: NodeInterface,
    out_target: NodeInterface
}

impl ForwardNode {
    pub fn new (gl: &Gl) -> Self {
        let mut viewport_data: [i32; 4] = [0,0,0,0];
        gl.get_integer_v(GL_VIEWPORT, &mut viewport_data);

        let win_size = Vector2f::new(viewport_data[2] as f32, viewport_data[3] as f32);

        let render_target = RenderTarget::new(gl, win_size.x() as GLsizei, win_size.y() as GLsizei);

        Self {
            win_size,
            in_shader: NodeInterface {
                name: "in_shader".to_string(),
                data: None,
            },
            in_scene: NodeInterface{
                name: "in_scene".to_string(),
                data: None,
            },
            in_camera: NodeInterface{
                name: "in_camera".to_string(),
                data: None,
            },
            in_wireframe: NodeInterface{
                name: "in_wireframe".to_string(),
                data: Some(Boolean(false)),
            },
            in_soft: NodeInterface{
                name: "in_soft".to_string(),
                data: Some(Boolean(true)),
            },
            in_tess_level: NodeInterface{
                name: "in_tess_level".to_string(),
                data: Some(Float(1.0)),
            },
            out_target: NodeInterface {
                name: "out_target".to_string(),
                data: Some(RenderTargetData(render_target)),
            }
        }
    }
}

impl RenderNode for ForwardNode {
    fn evaluate(&mut self, gl: &Gl) {
        if self.in_scene.data.is_none() ||
           self.in_camera.data.is_none() ||
           self.in_wireframe.data.is_none() ||
           self.in_tess_level.data.is_none() ||
           self.in_shader.data.is_none()
        {
            return;
        }

        let scene = match self.in_scene.data.as_ref().unwrap() {
            ConnexionData::Scene(s) => { s },
            _ => { return },
        };

        let camera = match self.in_camera.data.as_ref().unwrap() {
            ConnexionData::Camera(c) => { c },
            _ => { return },
        };

        let shader = match self.in_shader.data.as_ref().unwrap() {
            ConnexionData::Shader(s) => { s },
            _ => { return },
        };

        let wireframe = match self.in_wireframe.data.as_ref().unwrap() {
            ConnexionData::Boolean(w) => { w },
            _ => { &false },
        };

        let soft_shading = match self.in_soft.data.as_ref().unwrap() {
            ConnexionData::Boolean(w) => { w },
            _ => { &true },
        };

        let tess_level = match self.in_tess_level.data.as_ref().unwrap() {
            ConnexionData::Float(l) => { l },
            _ => { &1.0 },
        };

        let render_target = match self.out_target.data.as_ref().unwrap() {
            RenderTargetData(b) => { b },
            _ => { return; },
        };

        render_target.bind(gl);

        gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        gl.enable(GL_BLEND);
        gl.enable(GL_DEPTH_TEST);

        shader.use_program(&gl);

        let win_loc = shader.get_uniform_location(&gl, String::from("window_size"));
        win_loc.set_2f(&gl, self.win_size.x(), self.win_size.y());

        let tess_loc = shader.get_uniform_location(&gl, String::from("tess_level"));
        tess_loc.set_1f(&gl, *tess_level);

        let soft_loc = shader.get_uniform_location(&gl, String::from("soft_shading"));
        soft_loc.set_1b(&gl, *soft_shading);
        let wire_loc = shader.get_uniform_location(&gl, String::from("draw_wireframe"));
        wire_loc.set_1b(&gl, *wireframe);

        scene.draw(gl, camera, shader);

        gl.use_program(0);
        render_target.unbind(gl);
    }

    fn update(&mut self, gl: &Gl, _delta_time: f32) {
        let mut viewport_data: [i32; 4] = [0,0,0,0];
        gl.get_integer_v(GL_VIEWPORT, &mut viewport_data);

        if (viewport_data[2] - self.win_size.x() as i32).abs() > 0 ||
            (viewport_data[3] - self.win_size.y() as i32).abs() > 0
        {
            self.win_size = Vector2f::new(viewport_data[2] as f32, viewport_data[3] as f32);
            self.out_target.data = Some(RenderTargetData(RenderTarget::new(
                gl,
                self.win_size.x() as GLsizei,
                self.win_size.y() as GLsizei
            )));
        }
    }

    fn get_input_names(&self) -> Vec<String> {
        vec!(
            self.in_camera.name.clone(),
            self.in_scene.name.clone(),
            self.in_shader.name.clone(),
            self.in_tess_level.name.clone(),
            self.in_wireframe.name.clone(),
        )
    }

    fn set_input(&mut self, name: String, value: Option<ConnexionData>) {
        match value {
            None => {},
            Some(opt_value) => {
                if name == self.in_camera.name {
                    match opt_value {
                        ConnexionData::Camera(_) => {self.in_camera.data = Some(opt_value)},
                        _ => {},
                    }
                }
                else if name == self.in_scene.name {
                    match opt_value {
                        ConnexionData::Scene(_) => {self.in_scene.data = Some(opt_value)},
                        _ => {},
                    }
                }
                else if name == self.in_shader.name {
                    match opt_value {
                        ConnexionData::Shader(_) => {self.in_shader.data = Some(opt_value)},
                        _ => {},
                    }
                }
                else if name == self.in_tess_level.name {
                    match opt_value {
                        ConnexionData::Float(_) => {self.in_tess_level.data = Some(opt_value)},
                        _ => {},
                    }
                }
                else if name == self.in_wireframe.name {
                    match opt_value {
                        ConnexionData::Boolean(_) => {self.in_wireframe.data = Some(opt_value)},
                        _ => {},
                    }
                }
                else if name == self.in_soft.name {
                    match opt_value {
                        ConnexionData::Boolean(_) => {self.in_soft.data = Some(opt_value)},
                        _ => {},
                    }
                }
            },
        }
    }

    fn get_output_names(&self) -> Vec<String> {
        vec!(self.out_target.name.clone())
    }

    fn get_output(&self, name: String) -> Option<&NodeInterface> {
        if name == self.out_target.name {
            Some(&self.out_target)
        } else {
            None
        }
    }
}