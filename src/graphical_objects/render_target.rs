use crate::gl::buffers::{FBO, RBO};
use crate::gl::texture::Texture;
use crate::gl::Gl;
use crate::gl::gl_enum::{GLsizei, GL_RGB, GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_TEXTURE_MAG_FILTER, GL_LINEAR, GL_COLOR_ATTACHMENT0, GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL_ATTACHMENT};

#[derive(Clone)]
pub struct RenderTarget {
    fbo: FBO,
    rbo: RBO,
    tex: Texture,
}

impl RenderTarget {
    pub fn new (gl: &Gl, width: GLsizei, height: GLsizei) -> Self {
        let fbo = FBO::new(gl);
        fbo.bind(gl);

        let tex = Texture::new(gl, width, height, GL_RGB);
        tex.bind(gl, GL_TEXTURE_2D);
        tex.set_data(gl, GL_TEXTURE_2D, None);
        gl.tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        gl.tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        tex.unbind(gl, GL_TEXTURE_2D);
        fbo.add_texture_attachment(gl, GL_COLOR_ATTACHMENT0, &tex, 0);

        let rbo = RBO::new(gl);
        rbo.bind(gl);
        rbo.set_storage(gl, GL_DEPTH24_STENCIL8, width, height);
        rbo.unbind(gl);
        fbo.add_rbo_attachment(gl, GL_DEPTH_STENCIL_ATTACHMENT, &rbo);

        fbo.unbind(gl);

        Self {
            fbo,
            rbo,
            tex
        }
    }

    pub fn texture (&self) -> &Texture {
        &self.tex
    }

    pub fn bind (&self, gl: &Gl) {
        self.fbo.bind(gl);
    }

    pub fn unbind (&self, gl: &Gl) {
        self.fbo.unbind(gl);
    }

    pub fn clean (&self, gl: &Gl) {
        self.rbo.clean(gl);
        self.fbo.clean(gl);
    }
}