use crate::gl::Gl;
use crate::gl::gl_enum::{GLuint, GLint, GLenum, GLbool, GL_TRUE, GL_FALSE};

use sp_scene::sp_mesh::sp_math::{vector::*, matrix::*};

pub mod shader_program;
pub mod compute_program;

#[derive(Clone)]
pub struct Shader {
    pub shader_type: GLenum,
    pub id: GLuint,
    pub source_file: Option<String>,
    pub source_code: String,
}

#[derive(Clone)]
pub struct Uniform {
    pub id: GLint,
    pub shader_id: GLuint,
}

impl Shader {
    pub fn from_file (gl: &Gl, shader_type: GLenum, source_file: String) -> Self {
        let id : GLuint = gl.create_shader(shader_type);

        let source_code : String = std::fs::read_to_string(&source_file).unwrap();

        gl.shader_source(id, &[source_code.clone()]);

        match gl.compile_shader(id) {
            Err(msg) => println!("{} : {}", shader_type, msg),
            _ => println!("{} : Compilation successful.", source_file),
        };

        let source_file = Some(source_file); // Shadowing for type correctness
        Self {
            shader_type,
            id,
            source_file,
            source_code
        }
    }

    pub fn from_code (gl: &Gl, shader_type: GLenum, source_code: String) -> Self {
        let id : GLuint = gl.create_shader(shader_type);

        gl.shader_source(id, &[source_code.clone()]);

        match gl.compile_shader(id) {
            Err(msg) => println!("{}", msg),
            Ok(_) => {}
        };

        let source_file = None; // Shadowing for type correctness
        Self {
            shader_type,
            id,
            source_file,
            source_code
        }
    }

    pub fn reload (&self, gl: &Gl) -> Self {
        match &self.source_file {
            Some(file) => Self::from_file(gl, self.shader_type, file.clone()),
            None => Self::from_code(gl, self.shader_type, self.source_code.clone())
        }
    }
}

fn _bool_to_glboolean (v: bool) -> GLbool {
    if v {GL_TRUE} else {GL_FALSE}
}

impl Uniform {

    pub fn set_1b (&self, gl: &Gl, value: bool) {
        gl.uniform_1i(self.id, if value {GL_TRUE as i32} else {GL_FALSE as i32});
    }

    pub fn set_1f (&self, gl: &Gl, value: f32) {
        gl.uniform_1f(self.id, value);
    }

    pub fn set_2f (&self, gl: &Gl, v1: f32, v2: f32) {
        gl.uniform_2f(self.id, v1, v2);
    }

    pub fn set_3f (&self, gl: &Gl, v1: f32, v2: f32, v3: f32) {
        gl.uniform_3f(self.id, v1, v2, v3);
    }

    pub fn set_4f (&self, gl: &Gl, v1: f32, v2: f32, v3: f32, v4: f32) {
        gl.uniform_4f(self.id, v1, v2, v3, v4);
    }

    pub fn set_1i (&self, gl: &Gl, value: i32) {
        gl.uniform_1i(self.id, value);
    }

    pub fn set_2i (&self, gl: &Gl, v1: i32, v2: i32) {
        gl.uniform_2i(self.id, v1, v2);
    }

    pub fn set_3i (&self, gl: &Gl, v1: i32, v2: i32, v3: i32) {
        gl.uniform_3i(self.id, v1, v2, v3);
    }

    pub fn set_4i (&self, gl: &Gl, v1: i32, v2: i32, v3: i32, v4: i32) {
        gl.uniform_4i(self.id, v1, v2, v3, v4);
    }

    pub fn set_1ui (&self, gl: &Gl, value: u32) {
        gl.uniform_1ui(self.id, value);
    }

    pub fn set_2ui (&self, gl: &Gl, v1: u32, v2: u32) {
        gl.uniform_2ui(self.id, v1, v2);
    }

    pub fn set_3ui (&self, gl: &Gl, v1: u32, v2: u32, v3: u32) {
        gl.uniform_3ui(self.id, v1, v2, v3);
    }

    pub fn set_4ui (&self, gl: &Gl, v1: u32, v2: u32, v3: u32, v4: u32) {
        gl.uniform_4ui(self.id, v1, v2, v3, v4);
    }

    pub fn set_2fv (&self, gl: &Gl, value: &Vector2f) {
        gl.uniform_2fv(self.id, 1,value.as_slice());
    }

    pub fn set_3fv (&self, gl: &Gl, value: &Vector3f) {
        gl.uniform_3fv(self.id, 1,value.as_slice());
    }

    pub fn set_4fv (&self, gl: &Gl, value: &Vector4f) {
        gl.uniform_4fv(self.id, 1,value.as_slice());
    }

    pub fn set_2iv (&self, gl: &Gl, value: &Vector2i) {
        gl.uniform_2iv(self.id, 1,value.as_slice());
    }

    pub fn set_3iv (&self, gl: &Gl, value: &Vector3i) {
        gl.uniform_3iv(self.id, 1,value.as_slice());
    }

    pub fn set_4iv (&self, gl: &Gl, value: &Vector4i) {
        gl.uniform_4iv(self.id, 1,value.as_slice());
    }

    pub fn set_2uiv (&self, gl: &Gl, value: &Vector2u) {
        gl.uniform_2uiv(self.id, 1,value.as_slice());
    }

    pub fn set_3uiv (&self, gl: &Gl, value: &Vector3u) {
        gl.uniform_3uiv(self.id, 1,value.as_slice());
    }

    pub fn set_4uiv (&self, gl: &Gl, value: &Vector4u) {
        gl.uniform_4uiv(self.id, 1,value.as_slice());
    }

    pub fn set_mat_2f (&self, gl: &Gl, transpose: bool, value: &Matrix2f) {
        gl.uniform_matrix_2fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }

    pub fn set_mat_3f (&self, gl: &Gl, transpose: bool, value: &Matrix3f) {
        gl.uniform_matrix_3fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }

    pub fn set_mat_4f (&self, gl: &Gl, transpose: bool, value: &Matrix4f) {
        gl.uniform_matrix_4fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }

    pub fn set_mat_2x3f (&self, gl: &Gl, transpose: bool, value: &Matrix2x3f) {
        gl.uniform_matrix_2x3fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }

    pub fn set_mat_3x2f (&self, gl: &Gl, transpose: bool, value: &Matrix3x2f) {
        gl.uniform_matrix_3x2fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }

    pub fn set_mat_2x4f (&self, gl: &Gl, transpose: bool, value: &Matrix2x4f) {
        gl.uniform_matrix_2x4fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }

    pub fn set_mat_4x2f (&self, gl: &Gl, transpose: bool, value: &Matrix4x2f) {
        gl.uniform_matrix_4x2fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }

    pub fn set_mat_3x4f (&self, gl: &Gl, transpose: bool, value: &Matrix3x4f) {
        gl.uniform_matrix_3x4fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }

    pub fn set_mat_4x3f (&self, gl: &Gl, transpose: bool, value: &Matrix4x3f) {
        gl.uniform_matrix_4x3fv(self.id, 1, _bool_to_glboolean(transpose), value.as_slice());
    }
}