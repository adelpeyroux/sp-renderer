use crate::gl::Gl;
use crate::gl::gl_enum::{GLuint, GL_COMPUTE_SHADER};

use super::Shader;

pub struct ComputeProgram {
    pub id: GLuint,
    compute_shader: Shader,
}

impl ComputeProgram {
    pub fn new (gl: &Gl, compute_file: String) -> Self {
        let id: GLuint = gl.create_program();

        let compute_shader = Shader::from_file(gl, GL_COMPUTE_SHADER, compute_file);

        Self {
            id,
            compute_shader
        }
    }

    pub fn attach_shaders (&self, gl: &Gl) {
        gl.attach_shader(self.id, self.compute_shader.id);
    }

    pub fn reload (&self, gl: &Gl) {
        self.compute_shader.reload(gl);
    }

    pub fn link (&self, gl: &Gl) {
        gl.link_program(self.id);
    }

    pub fn use_program (&self, gl: &Gl) {
        gl.use_program(self.id);
    }

    pub fn get_attrib_location (&self, gl: &Gl, attrib_name: String) -> GLuint {
        gl.get_attrib_location(self.id, attrib_name)
    }
}