use glutin::{self, PossiblyCurrent};

pub mod shader;
pub mod texture;
pub mod buffers;

use std::ffi::{CStr, CString};

pub mod gl_enum;
use gl_enum::*;
use std::ptr::null;

macro_rules! log {
    ($err_type: expr, $context: expr, $message: expr) => {
        println!("[{}] - {} - {}", $err_type, $context, $message);
    }
}

mod _gl {
    include!(concat!(env!("OUT_DIR"), "/gl_bindings.rs"));
}

pub struct Gl {
    pub gl: _gl::Gl,
}

impl Gl {
    pub fn initialize (gl_context: &glutin::Context<PossiblyCurrent>) -> Self {
        let gl = _gl::Gl::load_with(|ptr| gl_context.get_proc_address(ptr) as *const _);

        Self {
            gl,
        }
    }

    pub fn viewport (&self, x: GLint, y: GLint, width: GLint, height: GLint) {
        unsafe {
            self.gl.Viewport(x, y, width, height);
            self.check_error("viewport".to_string());
        }
    }

    pub fn finish (&self) {
        unsafe {
            self.gl.Finish();
            self.check_error("finish".to_string());
        };
    }

    pub fn enable (&self, cap: GLenum) {
        unsafe {
            self.gl.Enable(cap);
            self.check_error("enable".to_string());
        };
    }

    pub fn disable (&self, cap: GLenum) {
        unsafe {
            self.gl.Disable(cap);
            self.check_error("disable".to_string());
        };
    }

    fn get_error (&self) -> GLenum {
        unsafe {
            self.gl.GetError()
        }
    }

    #[cfg(debug_assertions)]
    fn check_error (&self, context: String) {
        match self.get_error() {
            GL_INVALID_ENUM => {
                log!("OGL".to_string(), context, "GL_INVALID_ENUM".to_string());
            },
            GL_INVALID_VALUE => {
                log!("OGL".to_string(), context, "GL_INVALID_VALUE".to_string());
            },
            GL_INVALID_OPERATION => {
                log!("OGL".to_string(), context, "GL_INVALID_OPERATION".to_string());
            },
            GL_OUT_OF_MEMORY => {
                log!("OGL".to_string(), context, "GL_OUT_OF_MEMORY".to_string());
            },
            GL_STACK_OVERFLOW => {
                log!("OGL".to_string(), context, "GL_STACK_OVERFLOW".to_string());
            },
            GL_STACK_UNDERFLOW => {
                log!("OGL".to_string(), context, "GL_STACK_UNDERFLOW".to_string());
            },
            GL_INVALID_FRAMEBUFFER_OPERATION => {
                log!("OGL".to_string(), context, "GL_INVALID_FRAMEBUFFER_OPERATION".to_string());
            },
            _ => {}
        };
    }

    #[cfg(not(debug_assertions))]
    fn check_error (&self, _context: String) {
        match self.get_error() {
            _ => {}
        };
    }

    pub fn blend_func (&self, sfactor: GLenum, dfactor: GLenum) {
        unsafe {
            self.gl.BlendFunc(sfactor, dfactor);
        };
        self.check_error("blend_func".to_string());
    }

    pub fn clear (&self, mask: GLenum) {
        unsafe {
            self.gl.Clear(mask);
        };
        self.check_error("clear".to_string());
    }

    pub fn clear_color (&self, r: f32, g: f32, b: f32, a: f32) {
        unsafe {
            self.gl.ClearColor(r, g, b, a);
        };
        self.check_error("clear_color".to_string());
    }

    pub fn get_integer_v (&self, pname: GLenum, data: &mut [GLint]) {
        unsafe {
            self.gl.GetIntegerv(pname, data.as_ptr() as *mut _);
        };
        self.check_error("get_integer_v".to_string());
    }

    pub fn get_string (&self, name: GLenum) -> String {
        unsafe {
            let data = CStr::from_ptr(self.gl.GetString(name) as *const _)
                .to_bytes()
                .to_vec();

            self.check_error("get_string".to_string());
            String::from_utf8(data).unwrap()
        }
    }

    pub fn get_string_i (&self, name: GLenum, index : u32) -> String {
        unsafe {
            let data = CStr::from_ptr(self.gl.GetStringi(name, index) as *const _)
                .to_bytes()
                .to_vec();
            self.check_error("get_string_i".to_string());
            String::from_utf8(data).unwrap()
        }
    }

    // Shader functions
    pub fn create_shader (&self, shader_type : GLenum) -> GLuint {
        unsafe {
            let id = self.gl.CreateShader(shader_type);

            self.check_error("create_shader".to_string());
            id
        }
    }

    pub fn shader_source (&self, shader: GLuint, strings: &[String]) {
        unsafe {
            let lengths: std::vec::Vec<GLint> = strings.iter().map(| s | s.len() as GLint).collect();
            self.gl.ShaderSource(
                shader,
                strings.len() as GLsizei,
                strings.as_ptr() as *const _,
                lengths.as_ptr() as *const _,
            );
        };
        self.check_error("create_shader".to_string());
    }

    pub fn compile_shader (&self, shader: GLuint) -> Result<(), String> {
        unsafe {
            self.gl.CompileShader(shader);

            self.check_error("compile_shader".to_string());
            let mut success : GLint = 1;
            self.gl.GetShaderiv(shader, _gl::COMPILE_STATUS, &mut success);

            if success == 0 {
                let mut len: GLint = 0;
                self.gl.GetShaderiv(shader, _gl::INFO_LOG_LENGTH, &mut len);

                let mut buffer: Vec<u8> = Vec::with_capacity(len as usize + 1);

                buffer.extend([b' '].iter().cycle().take(len as usize));
                let error: CString = CString::from_vec_unchecked(buffer);

                self.gl.GetShaderInfoLog(
                  shader,
                  len,
                  std::ptr::null_mut(),
                  error.as_ptr() as *mut GLchar
                );

                return Err(error.to_string_lossy().into_owned());
            }
        }

        return Ok(());
    }

    pub fn create_program (&self) -> GLuint {
        unsafe {
            let id = self.gl.CreateProgram();
            self.check_error("create_program".to_string());

            id
        }
    }

    pub fn attach_shader (&self, program: GLuint, shader: GLuint) {
        unsafe {
            self.gl.AttachShader(program, shader);
            self.check_error("attach_shader".to_string());
        };
    }

    pub fn detach_shader (&self, program: GLuint, shader: GLuint) {
        unsafe {
            self.gl.DetachShader(program, shader);
            self.check_error("detach_shader".to_string());
        };
    }

    pub fn delete_shader (&self, shader: GLuint) {
        unsafe {
            self.gl.DeleteShader(shader);
            self.check_error("delete_shader".to_string());
        };
    }

    pub fn delete_program (&self, program: GLuint) {
        unsafe {
            self.gl.DeleteProgram(program);
            self.check_error("delete_program".to_string());
        };
    }

    pub fn link_program (&self, program: GLuint) {
        unsafe {
            self.gl.LinkProgram(program);
            self.check_error("link_program".to_string());
        };
    }

    pub fn use_program (&self, program: GLuint) {
        unsafe {
            self.gl.UseProgram(program);
            self.check_error("use_program".to_string());
        };
    }

    // Uniform functions
    pub fn get_uniform_location (&self, program: GLuint, name: String) -> GLint {
        unsafe {
            let loc = self.gl.GetUniformLocation(program, name.as_ptr() as *const _);
            self.check_error("get_uniform_location".to_string());

            loc
        }
    }

    pub fn uniform_1f (&self, location: GLint, v0: GLfloat) {
        unsafe {
            self.gl.Uniform1f(location, v0);
            self.check_error("get_uniform_location".to_string());
        };
    }

    pub fn uniform_1fv (&self, location: GLint, count: GLsizei, v: &[GLfloat]) {
        unsafe {
            self.gl.Uniform1fv(location, count, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_1fv".to_string());
        };
    }

    pub fn uniform_2f (&self, location: GLint, v0: GLfloat, v1: GLfloat) {
        unsafe {
            self.gl.Uniform2f(location, v0, v1);
            self.check_error("uniform_2f".to_string());
        };
    }

    pub fn uniform_2fv (&self, location: GLint, count: GLsizei, v: &[GLfloat]) {
        unsafe {
            self.gl.Uniform2fv(location, count, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_2fv".to_string());
        };
    }

    pub fn uniform_3f (&self, location: GLint, v0: GLfloat, v1: GLfloat, v2: GLfloat) {
        unsafe {
            self.gl.Uniform3f(location, v0, v1, v2);
            self.check_error("uniform_3f".to_string());
        };
    }

    pub fn uniform_3fv (&self, location: GLint, count: GLsizei, v: &[GLfloat]) {
        unsafe {
            self.gl.Uniform3fv(location, count, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_3fv".to_string());
        };
    }

    pub fn uniform_4f (&self, location: GLint, v0: GLfloat, v1: GLfloat, v2: GLfloat, v3: GLfloat) {
        unsafe {
            self.gl.Uniform4f(location, v0, v1, v2, v3);
            self.check_error("uniform_4f".to_string());
        };
    }

    pub fn uniform_4fv (&self, location: GLint, count: GLsizei, v: &[GLfloat]) {
        unsafe {
            self.gl.Uniform4fv(location, count, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_4fv".to_string());
        };
    }

    pub fn uniform_1i (&self, location: GLint, v0: GLint) {
        unsafe {
            self.gl.Uniform1i(location, v0);
            self.check_error("uniform_1i".to_string());
        };
    }

    pub fn uniform_1iv (&self, location: GLint, count: GLsizei, v: &[GLint]) {
        unsafe {
            self.gl.Uniform1iv(location, count, v.as_ptr() as *const GLint);
            self.check_error("uniform_1iv".to_string());
        };
    }

    pub fn uniform_2i (&self, location: GLint, v0: GLint, v1: GLint) {
        unsafe {
            self.gl.Uniform2i(location, v0, v1);
            self.check_error("uniform_2i".to_string());
        };
    }

    pub fn uniform_2iv (&self, location: GLint, count: GLsizei, v: &[GLint]) {
        unsafe {
            self.gl.Uniform2iv(location, count, v.as_ptr() as *const GLint);
            self.check_error("uniform_2iv".to_string());
        };
    }

    pub fn uniform_3i (&self, location: GLint, v0: GLint, v1: GLint, v2: GLint) {
        unsafe {
            self.gl.Uniform3i(location, v0, v1, v2);
            self.check_error("uniform_3i".to_string());
        };
    }

    pub fn uniform_3iv (&self, location: GLint, count: GLsizei, v: &[GLint]) {
        unsafe {
            self.gl.Uniform3iv(location, count, v.as_ptr() as *const GLint);
            self.check_error("uniform_3iv".to_string());
        };
    }

    pub fn uniform_4i (&self, location: GLint, v0: GLint, v1: GLint, v2: GLint, v3: GLint) {
        unsafe {
            self.gl.Uniform4i(location, v0, v1, v2, v3);
            self.check_error("uniform_4i".to_string());
        };
    }

    pub fn uniform_4iv (&self, location: GLint, count: GLsizei, v: &[GLint]) {
        unsafe {
            self.gl.Uniform4iv(location, count, v.as_ptr() as *const GLint);
            self.check_error("uniform_4iv".to_string());
        };
    }

    pub fn uniform_1ui (&self, location: GLint, v0: GLuint) {
        unsafe {
            self.gl.Uniform1ui(location, v0);
            self.check_error("uniform_1ui".to_string());
        };
    }

    pub fn uniform_1uiv (&self, location: GLint, count: GLsizei, v: &[GLuint]) {
        unsafe {
            self.gl.Uniform1uiv(location, count, v.as_ptr() as *const GLuint);
            self.check_error("uniform_1uiv".to_string());
        };
    }

    pub fn uniform_2ui (&self, location: GLint, v0: GLuint, v1: GLuint) {
        unsafe {
            self.gl.Uniform2ui(location, v0, v1);
            self.check_error("uniform_2ui".to_string());
        };
    }

    pub fn uniform_2uiv (&self, location: GLint, count: GLsizei, v: &[GLuint]) {
        unsafe {
            self.gl.Uniform2uiv(location, count, v.as_ptr() as *const GLuint);
            self.check_error("uniform_2uiv".to_string());
        };
    }

    pub fn uniform_3ui (&self, location: GLint, v0: GLuint, v1: GLuint, v2: GLuint) {
        unsafe {
            self.gl.Uniform3ui(location, v0, v1, v2);
            self.check_error("uniform_3ui".to_string());
        };
    }

    pub fn uniform_3uiv (&self, location: GLint, count: GLsizei, v: &[GLuint]) {
        unsafe {
            self.gl.Uniform3uiv(location, count, v.as_ptr() as *const GLuint);
            self.check_error("uniform_3uiv".to_string());
        };
    }

    pub fn uniform_4ui (&self, location: GLint, v0: GLuint, v1: GLuint, v2: GLuint, v3: GLuint) {
        unsafe {
            self.gl.Uniform4ui(location, v0, v1, v2, v3);
            self.check_error("uniform_4ui".to_string());
        };
    }

    pub fn uniform_4uiv (&self, location: GLint, count: GLsizei, v: &[GLuint]) {
        unsafe {
            self.gl.Uniform4uiv(location, count, v.as_ptr() as *const GLuint);
            self.check_error("uniform_4uiv".to_string());
        };
    }

    pub fn uniform_matrix_2fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix2fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_matrix_2fv".to_string());
        };
    }

    pub fn uniform_matrix_3fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix3fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_matrix_3fv".to_string());
        };
    }

    pub fn uniform_matrix_4fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix4fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_matrix_4fv".to_string());
        };
    }

    pub fn uniform_matrix_2x3fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix2x3fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_matrix_2x3fv".to_string());
        };
    }

    pub fn uniform_matrix_3x2fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix3x2fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("UniformMatrix3x2fv".to_string());
        };
    }

    pub fn uniform_matrix_2x4fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix2x4fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("UniformMatrix2x4fv".to_string());
        };
    }

    pub fn uniform_matrix_4x2fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix4x2fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_matrix_4x2fv".to_string());
        };
    }

    pub fn uniform_matrix_3x4fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix3x4fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_matrix_3x4fv".to_string());
        };
    }

    pub fn uniform_matrix_4x3fv (&self, location: GLint, count: GLsizei, transpose: GLbool, v: &[GLfloat]) {
        unsafe {
            self.gl.UniformMatrix4x3fv(location, count, transpose, v.as_ptr() as *const GLfloat);
            self.check_error("uniform_matrix_4x3fv".to_string());
        };
    }

    // Buffer functions
    pub fn gen_buffers (&self, n: GLsizei, buffers: &mut [GLuint]) {
        unsafe {
            self.gl.GenBuffers(n, buffers.as_ptr() as *mut GLuint);
            self.check_error("gen_buffers".to_string());
        };
    }

    pub fn gen_vertex_arrays (&self, n: GLsizei, buffers: &mut [GLuint]) {
        unsafe {
            self.gl.GenVertexArrays(n, buffers.as_ptr() as *mut GLuint);
            self.check_error("gen_vertex_arrays".to_string());
        };
    }

    pub fn bind_buffer (&self, target: GLenum, buffer: GLuint) {
        unsafe {
            self.gl.BindBuffer(target, buffer);
            self.check_error("bind_buffer".to_string());
        };
    }

    pub fn bind_vertex_array (&self, buffer: GLuint) {
        unsafe {
            self.gl.BindVertexArray(buffer);
            self.check_error("bind_vertex_array".to_string());
        };
    }

    pub fn buffer_data<T> (&self, target: GLenum, data: &[T], usage: GLenum) {
        unsafe {
            self.gl.BufferData(
                target,
                (data.len() * std::mem::size_of::<T>()) as GLsizeiptr,
                data.as_ptr() as *const _,
                usage
            );
            self.check_error("buffer_data".to_string());
        };
    }

    pub fn named_buffer_data<T> (&self, buffer: GLuint, data: &[T], usage: GLenum) {
        unsafe {
            self.gl.NamedBufferData(
                buffer,
                (data.len() * std::mem::size_of::<T>()) as GLsizeiptr,
                data.as_ptr() as *const _,
                usage
            );
            self.check_error("named_buffer_data".to_string());
        };
    }

    pub fn delete_buffers (&self, n: GLsizei, ids: &[GLuint]) {
        unsafe {
            self.gl.DeleteBuffers(
                n,
                ids.as_ptr() as *const GLuint,
            );
            self.check_error("delete_buffers".to_string());
        };
    }

    pub fn gen_frame_buffers (&self, n: GLsizei, buffers: &mut [GLuint]) {
        unsafe {
            self.gl.GenFramebuffers(n, buffers.as_ptr() as *mut GLuint);
            self.check_error("gen_frame_buffers".to_string());
        };
    }

    pub fn bind_frame_buffer (&self, target: GLenum, buffer: GLuint) {
        unsafe {
            self.gl.BindFramebuffer(target, buffer);
            self.check_error("bind_frame_buffer".to_string());
        };
    }

    pub fn delete_frame_buffers (&self, n: GLsizei, ids: &[GLuint]) {
        unsafe {
            self.gl.DeleteFramebuffers(
                n,
                ids.as_ptr() as *const GLuint,
            );
            self.check_error("delete_frame_buffers".to_string());
        };
    }

    pub fn gen_render_buffers (&self, n: GLsizei, buffers: &mut [GLuint]) {
        unsafe {
            self.gl.GenRenderbuffers(n, buffers.as_ptr() as *mut GLuint);
            self.check_error("gen_render_buffers".to_string());
        };
    }

    pub fn bind_render_buffer (&self, target: GLenum, buffer: GLuint) {
        unsafe {
            self.gl.BindRenderbuffer(target, buffer);
            self.check_error("bind_render_buffer".to_string());
        };
    }

    pub fn delete_render_buffers (&self, n: GLsizei, ids: &[GLuint]) {
        unsafe {
            self.gl.DeleteRenderbuffers(
                n,
                ids.as_ptr() as *const GLuint,
            );
            self.check_error("delete_render_buffers".to_string());
        };
    }

    pub fn render_buffer_storage (&self, target: GLenum, internal_format: GLenum, width: GLsizei, height: GLsizei) {
        unsafe {
            self.gl.RenderbufferStorage (
                target,
                internal_format,
                width,
                height
            );
            self.check_error("render_buffer_storage".to_string());
        };
    }

    pub fn read_buffer (&self, mode: GLenum) {
        unsafe {
            self.gl.ReadBuffer(mode);
            self.check_error("read_buffer".to_string());
        }
    }

    pub fn draw_buffer (&self, mode: GLenum) {
        unsafe {
            self.gl.DrawBuffer(mode);
            self.check_error("draw_buffer".to_string());
        }
    }

    /*pub fn blit_frame_buffer (
        &self,
        src_x0: GLint, src_y0: GLint,
        src_x1: GLint, src_y1: GLint,
        dst_x0: GLint, dst_y0: GLint,
        dst_x1: GLint, dst_y1: GLint,
        mask: GLbitfield,
        filter: GLenum,
    ) {

    }*/

    pub fn frame_buffer_texture_2d (&self, target: GLenum, attachment: GLenum, textarget: GLenum, texture: GLuint, level: GLint) {
        unsafe {
            self.gl.FramebufferTexture2D(
                target,
                attachment,
                textarget,
                texture,
                level,
            );
            self.check_error("frame_buffer_texture_2d".to_string());
        };
    }

    pub fn frame_buffer_render_buffer (&self, target: GLenum, attachment: GLenum, render_buffer_target: GLenum, render_buffer: GLuint) {
        unsafe {
            self.gl.FramebufferRenderbuffer(
                target,
                attachment,
                render_buffer_target,
                render_buffer
            );
            self.check_error("frame_buffer_render_buffer".to_string());
        };
    }

    // Textures functions
    pub fn gen_textures (&self, n: GLsizei, tex: &mut [GLuint]) {
        unsafe {
            self.gl.GenTextures(n, tex.as_ptr() as *mut GLuint);
            self.check_error("gen_textures".to_string());
        };
    }

    pub fn bind_texture (&self, target: GLenum, texture: GLuint) {
        unsafe {
            self.gl.BindTexture(target, texture);
            self.check_error("bind_texture".to_string());
        };
    }

    pub fn tex_image_2d <T>(&self, target: GLenum, level: GLint, internal_format: GLenum,
                            width: GLsizei, height: GLsizei, border: GLint, format: GLenum,
                            data_type: GLenum, data: Option<&[T]>) {
        unsafe {
            self.gl.TexImage2D(
                target, level, internal_format as GLint, width, height, border,
                format, data_type
                , match data {
                    Some(d) => d.as_ptr() as *const _,
                    None => null(),
                }
            );
            self.check_error("tex_image_2d".to_string());
        };
    }

    pub fn tex_parameter_f (&self, target: GLenum, pname: GLenum, value: GLfloat) {
        unsafe {
          self.gl.TexParameterf(target, pname, value);
            self.check_error("tex_parameter_f".to_string());
        };
    }

    pub fn tex_parameter_i (&self, target: GLenum, pname: GLenum, value: GLuint) {
        unsafe {
            self.gl.TexParameteri(target, pname, value as GLint);
            self.check_error("tex_parameter_i".to_string());
        };
    }

    pub fn texture_parameter_f (&self, texture: GLuint, pname: GLenum, value: GLfloat) {
        unsafe {
            self.gl.TextureParameterf(texture, pname, value);
            self.check_error("texture_parameter_f".to_string());
        };
    }

    pub fn texture_parameter_i (&self, texture: GLuint, pname: GLenum, value: GLint) {
        unsafe {
            self.gl.TextureParameteri(texture, pname, value);
            self.check_error("texture_parameter_i".to_string());
        };
    }

    pub fn tex_parameter_fv (&self, target: GLenum, pname: GLenum, values: &[GLfloat]) {
        unsafe {
            self.gl.TexParameterfv(target, pname, values.as_ptr() as *const _);
            self.check_error("tex_parameter_fv".to_string());
        };
    }

    pub fn tex_parameter_iv (&self, target: GLenum, pname: GLenum, values: &[GLint]) {
        unsafe {
            self.gl.TexParameteriv(target, pname, values.as_ptr() as *const _);
            self.check_error("tex_parameter_iv".to_string());
        };
    }

    pub fn tex_parameter_liv (&self, target: GLenum, pname: GLenum, values: &[GLint]) {
        unsafe {
            self.gl.TexParameterIiv(target, pname, values.as_ptr() as *const _);
            self.check_error("tex_parameter_liv".to_string());
        };
    }

    pub fn tex_parameter_luiv (&self, target: GLenum, pname: GLenum, values: &[GLint]) {
        unsafe {
            self.gl.TexParameterIuiv(target, pname, values.as_ptr() as *const _);
            self.check_error("tex_parameter_luiv".to_string());
        };
    }

    pub fn texture_parameter_fv (&self, texture: GLuint, pname: GLenum, values: &[GLfloat]) {
        unsafe {
            self.gl.TextureParameterfv(texture, pname, values.as_ptr() as *const _);
            self.check_error("texture_parameter_fv".to_string());
        };
    }

    pub fn texture_parameter_iv (&self, texture: GLuint, pname: GLenum, values: &[GLint]) {
        unsafe {
            self.gl.TextureParameteriv(texture, pname, values.as_ptr() as *const _);
            self.check_error("texture_parameter_iv".to_string());
        };
    }

    pub fn texture_parameter_liv (&self, texture: GLuint, pname: GLenum, values: &[GLint]) {
        unsafe {
            self.gl.TextureParameterIiv(texture, pname, values.as_ptr() as *const _);
            self.check_error("texture_parameter_liv".to_string());
        };
    }

    pub fn texture_parameter_luiv (&self, texture: GLuint, pname: GLenum, values: &[GLint]) {
        unsafe {
            self.gl.TextureParameterIuiv(texture, pname, values.as_ptr() as *const _);
            self.check_error("texture_parameter_luiv".to_string());
        };
    }

    pub fn generate_mipmap (&self, target: GLenum) {
        unsafe {
            self.gl.GenerateMipmap(target);
            self.check_error("generate_mipmap".to_string());
        }
    }

    pub fn generate_texture_mipmap (&self, texture: GLuint) {
        unsafe {
            self.gl.GenerateTextureMipmap(texture);
            self.check_error("generate_texture_mipmap".to_string());
        };
    }

    pub fn active_texture (&self, texture: GLenum) {
        unsafe {
            self.gl.ActiveTexture(texture);
            self.check_error("active_texture".to_string());
        };
    }

    // VertexAttrib functions
    pub fn get_attrib_location (&self, program: GLuint, name: String) -> GLuint {
        unsafe {
            let loc = self.gl.GetAttribLocation(program, name.as_ptr() as *const _) as GLuint;
            self.check_error("get_attrib_location".to_string());

            loc
        }
    }

    pub fn vertex_attrib_pointer<T> (&self, index: GLuint, size: GLint, _type: GLenum, normalized: GLbool, stride: GLsizei, pointer: GLuint) {
        unsafe {
            self.gl.VertexAttribPointer(
                index,
                size,
                _type,
                normalized,
                stride * std::mem::size_of::<T>() as GLsizei,
                (pointer as usize * std::mem::size_of::<T>()) as *const () as *const _
            );
            self.check_error("vertex_attrib_pointer".to_string());
        };
    }

    pub fn vertex_attrib_i_pointer<T> (&self, index: GLuint, size: GLint, _type: GLenum, stride: GLsizei, pointer: GLuint) {
        unsafe {
            self.gl.VertexAttribIPointer(
                index,
                size,
                _type,
                stride * std::mem::size_of::<T>() as GLsizei,
                (pointer as usize * std::mem::size_of::<T>()) as *const () as *const _
            );
            self.check_error("vertex_attrib_i_pointer".to_string());
        };
    }

    pub fn vertex_attrib_l_pointer<T> (&self, index: GLuint, size: GLint, _type: GLenum, stride: GLsizei, pointer: GLuint) {
        unsafe {
            self.gl.VertexAttribLPointer(
                index,
                size,
                _type,
                stride * std::mem::size_of::<T>() as GLsizei,
                (pointer as usize * std::mem::size_of::<T>()) as *const () as *const _
            );
            self.check_error("vertex_attrib_l_pointer".to_string());
        };
    }

    pub fn enable_vertex_attrib_array (&self, index: GLuint) {
        unsafe {
            self.gl.EnableVertexAttribArray(index);
            self.check_error("enable_vertex_attrib_array".to_string());
        };
    }

    pub fn disable_vertex_attrib_array (&self, index: GLuint) {
        unsafe {
            self.gl.DisableVertexAttribArray(index);
            self.check_error("disable_vertex_attrib_array".to_string());
        };
    }

    pub fn enable_vertex_array_attrib (&self, vaobj: GLuint, index: GLuint) {
        unsafe {
            self.gl.EnableVertexArrayAttrib(vaobj, index);
            self.check_error("enable_vertex_array_attrib".to_string());
        };
    }

    pub fn disable_vertex_array_attrib (&self, vaobj: GLuint, index: GLuint) {
        unsafe {
            self.gl.DisableVertexArrayAttrib(vaobj, index);
            self.check_error("disable_vertex_array_attrib".to_string());
        };
    }

    // Draw functions
    pub fn draw_arrays (&self, mode: GLenum, first: GLint, count: GLsizei) {
        unsafe {
            self.gl.DrawArrays(mode, first, count);
            self.check_error("draw_arrays".to_string());
        };
    }

    pub fn draw_elements (&self, mode: GLenum, count: GLsizei, _type: GLenum, indices: &[u32]) {
        unsafe {
            self.gl.DrawElements (
                mode,
                count,
                _type,
                indices.as_ptr() as *const _
            );
            self.check_error("draw_elements".to_string());
        };
    }
}


