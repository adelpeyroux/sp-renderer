pub extern crate sp_scene;

pub mod gl;
pub mod graphical_objects;
pub mod render_graph;
pub mod nodes;

use glutin::{self, PossiblyCurrent};

pub fn init (gl_context: &glutin::Context<PossiblyCurrent>) -> gl::Gl {
    gl::Gl::initialize(gl_context)
}